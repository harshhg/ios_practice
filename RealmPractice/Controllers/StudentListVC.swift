//
//  StudentListVC.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import UIKit
import RealmSwift

class StudentListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, filterDelegate , sortDelegate, clearFilter{
    func clearFilter(){
        self.viewDidLoad()
    }
    
    func applyFilter(nameFilter: String?) {
        filterStudentData = studentData.filter({$0.name.contains(nameFilter!)})
        studentData = filterStudentData
        self.tableView.reloadData()
    }
    
    func applySort(order: String?) {
        switch(order){
            case  "LTH" : filterStudentData = studentData.sorted { $0.age <  $1.age}
            case  "HTL" : filterStudentData = studentData.sorted { $0.age >  $1.age}
            default: break
        }
        studentData = filterStudentData
        self.tableView.reloadData()
    }
    
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBAction func filterBtnAction(_ sender: Any) {
        let filterVC = self.storyboard?.instantiateViewController(identifier: "FilterViewController") as! FilterViewController
        filterVC.modalPresentationStyle = .overCurrentContext
        filterVC.modalTransitionStyle = .crossDissolve
        filterVC.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.6)
        filterVC.delegateFilter = self
        filterVC.delegateSort = self
        filterVC.delegateClear = self
        present(filterVC, animated: true, completion: nil)
    }
    
    var studentData = [Student]()
    var filterStudentData = [Student]()
//    var filter: Bool =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
    
    
    
    
    // MARK :- Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "StudentListCell") as? StudentListCell
        
        let obj = studentData[indexPath.row]
        
        cell?.lblName.text = "Name - " + obj.name
        cell?.lblAge.text = "Age - " + obj.age
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let StudentDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "StudentDetailVC") as? StudentDetailVC
        let obj = studentData[indexPath.row]
        StudentDetailVC?.studentName = obj.name
        StudentDetailVC?.studentAge = obj.age
        StudentDetailVC?.studentAddress = obj.address
        StudentDetailVC?.studentContact = obj.contact

        self.navigationController?.pushViewController(StudentDetailVC!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func fetchData(){
        let records = DBManager.sharedInstance.getDataFromDB()
        studentData = [Student]()
        for r in records{
            studentData.append(r)
        }
        self.tableView.reloadData()
    }
    
    
    
    
}
