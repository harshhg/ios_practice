//
//  HomePageVC.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import UIKit

class HomePageVC: UIViewController {

    @IBOutlet weak var addStudentBtn: UIButton!
    @IBAction func addStudentBtnAction(_ sender: Any) {
        let addStudentVC = self.storyboard!.instantiateViewController(identifier: "AddStudentVC") as? AddStudentVC
        self.navigationController?.pushViewController(addStudentVC!, animated: true)
    }
    
    
    @IBOutlet weak var viewStudentBtn: UIButton!
    @IBAction func viewStudentBtnAction(_ sender: Any) {
        let studentListVC = self.storyboard!.instantiateViewController(identifier: "StudentListVC") as? StudentListVC
        self.navigationController?.pushViewController(studentListVC!, animated: true)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
