//
//  FilterViewController.swift
//  RealmPractice
//
//  Created by Auriga on 02/09/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import UIKit

protocol filterDelegate: class{
    func applyFilter(nameFilter : String?)
}
protocol sortDelegate: class{
    func applySort(order: String?)
}
protocol clearFilter: class{
    func clearFilter()
}

class FilterViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    weak var delegateFilter : filterDelegate?
    weak var delegateSort : sortDelegate?
    weak var delegateClear : clearFilter?
    
    @IBAction func goBtnAction(_ sender: Any) {
        delegateFilter?.applyFilter(nameFilter: nameTextField.text)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func LTH_Age_Action(_ sender: Any) {
        delegateSort?.applySort(order: "LTH")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func HTL_Age_Action(_ sender: Any) {
        delegateSort?.applySort(order: "HTL")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearFilterAction(_ sender: Any) {
        delegateClear?.clearFilter()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
