//
//  AddStudentVC.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import UIKit
import RealmSwift

class AddStudentVC: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var StudentID: UITextField!
    @IBOutlet weak var StudentName: UITextField!
    @IBOutlet weak var StudentAge: UITextField!
    @IBOutlet weak var StudentAddress: UITextField!
    @IBOutlet weak var StudentContact: UITextField!
    @IBOutlet weak var AddStudentBtn: UIButton!
    
    @IBOutlet weak var StudentImage: UIImageView!{
        didSet{
            StudentImage.layer.cornerRadius = StudentImage.frame.height/2
        }
    }
    @IBOutlet weak var uploadImgBtn: UIButton!
    @IBAction func uploadImgBtnAction(_ sender: Any) {
        if(UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    var imagePicker  = UIImagePickerController()
    // add student
    @IBAction func AddStudentBtnAction(_ sender: Any) {
        let student = Student()
        student.id = StudentID.text
        student.name = StudentName.text
        student.age = StudentAge.text
        student.address = StudentAddress.text
        student.contact = StudentContact.text
        
        guard let image = StudentImage.image else{
            return
        }
        student.path = imgPath
        print("image -", imgPath)
        DBManager.sharedInstance.delegate = self
        DBManager.sharedInstance.addStudent(object: student)
    }
    
    var imgPath : NSData?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        StudentImage.image = info[.originalImage] as? UIImage
        
//        var path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
//        let timestampFilename = String(Int(Date().timeIntervalSince1970)) + "_newImage.png"
//        let url = path?.appendingPathComponent(timestampFilename)
//        print(url)
//        let image =  info[.originalImage] as? UIImage
//        let imgurl = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL
//        let path = (imgurl?.path)!
//        let localPath = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(path)
//        let data = NSData(contentsOf:NSURL(string: "your image url")! as URL)
        
        //this block of code adds data to the above path
//        imgPath = localPath?.relativePath as! String
    }
}


extension AddStudentVC : RealmDelegate{
    func clearFields() {
        StudentID.text = ""
        StudentName.text = ""
        StudentContact.text = ""
        StudentAddress.text = ""
        StudentAge.text = ""
    }
}
