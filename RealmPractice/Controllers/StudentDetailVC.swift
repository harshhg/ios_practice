//
//  StudentDetailVC.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import UIKit

class StudentDetailVC: UIViewController {

    var studentName = "";
    var studentAge = ""
    var studentAddress = ""
    var studentContact = ""
    var path : NSData?
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var studentImage: UIImageView! {
        didSet{
            studentImage.layer.cornerRadius = studentImage.frame.height/2
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fillData()
//        print("load - ",path)

    }
    func fillData(){
        lblName.text = studentName
        lblAge.text = studentAge
        lblAddress.text = studentAddress
        lblContact.text = studentContact

    }
}
