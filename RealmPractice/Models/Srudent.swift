//
//  Srudent.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import RealmSwift

class Student : Object {
    @objc dynamic var id : String!
    @objc dynamic var name : String!
    @objc dynamic var age : String!
    @objc dynamic var address : String!
    @objc dynamic var contact : String!
     var path : NSData!
}
