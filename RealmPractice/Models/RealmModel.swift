//
//  RealmModel.swift
//  RealmPractice
//
//  Created by Auriga on 31/08/20.
//  Copyright © 2020 Auriga. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmDelegate {
    func clearFields()
}
extension RealmDelegate {
    func clearFields(){}
}

class DBManager {
    //MARK: Singleton
    static let sharedInstance = DBManager()
    var realm : Realm?
    var delegate : RealmDelegate?
    
    //MARK: Init
    private init(){
        do {
           realm = try Realm()
        } catch let err {
            print(err)
        }
    }
    
    //retrive data from db
   func getDataFromDB() -> [Student]{
        guard let results  = realm?.objects(Student.self) else { return [] }
        return Array(results)
    }
    
    //write an object in db
    func addStudent(object: Student){
        do {
            try realm?.write{
                realm?.add(object)
            print("Data Saved!")
            delegate?.clearFields()
            }
        } catch let err {
            print(err)
        }
    }
}

